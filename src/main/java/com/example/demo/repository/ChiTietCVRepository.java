package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.ChiTietCV;

public interface ChiTietCVRepository extends JpaRepository<ChiTietCV, Integer> {

}
