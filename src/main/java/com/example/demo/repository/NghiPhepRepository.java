package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.XinNghiPhep;

public interface NghiPhepRepository extends JpaRepository<XinNghiPhep, Integer> {
	@Query("SELECT x FROM XinNghiPhep x WHERE x.nhanvien.id = :id")
	List<XinNghiPhep> nghiPhepOfNhanVien(@Param("id") Integer id);

}
