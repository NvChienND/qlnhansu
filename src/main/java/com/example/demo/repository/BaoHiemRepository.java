package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.BaoHiem;

public interface BaoHiemRepository extends JpaRepository<BaoHiem, Integer>{
	@Query("SELECT b FROM BaoHiem b WHERE b.status = 1")
	List<BaoHiem> getAll();
}
