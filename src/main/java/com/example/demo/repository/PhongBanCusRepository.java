package com.example.demo.repository;

import java.util.List;

import com.example.demo.dto.NhanViendto;

public interface PhongBanCusRepository {
	List<NhanViendto> getNhanVienOfPhongBan(Integer id);
	
	List<NhanViendto> filterByName(Integer id_phongban, String name);
	
	List<NhanViendto> xinNghiPhep(Integer id);
	
	List<NhanViendto> thamGiaHoatDong(Integer id_h, Integer id_p);

}
