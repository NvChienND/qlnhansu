package com.example.demo.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.NhanViendto;
import com.example.demo.model.HoatDongNV;
import com.example.demo.repository.HoatDongNVRepository;
import com.example.demo.repository.PhongBanCusRepository;


@Repository("phongbancusrepo")
public class PhongBanCusRepositoryImpl implements PhongBanCusRepository {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private HoatDongNVRepository hoatDongRepo;
	
	@Override
	public List<NhanViendto> getNhanVienOfPhongBan(Integer id) {
		Integer idn;
		StringBuilder sql = new StringBuilder();
		sql.append("select nguoi.id as nguoi_id, nguoi.ten, nguoi.nam_sinh, nguoi.sdt, nhanvien.id as nhanvien_id, nhanvien.manv, nhanvien.vi_tri, baohiem.chi_phi from ");
		sql.append("phongban join nhanvien on phongban.id = nhanvien.phongban_id join baohiem on nhanvien.baohiem_id = baohiem.id ");
		sql.append("join nguoi on nguoi.id = nhanvien.nguoi_id where phongban.id = :id and nhanvien.status = :statusn ");
		Query query = (Query) em.createNativeQuery(sql.toString());
		query.setParameter("id", id);
		query.setParameter("statusn", "1");
		List<Object[]> list = query.getResultList();
		List<NhanViendto> listr = new ArrayList<>();
		for(Object[] o : list) {
			NhanViendto nhanViendto = new NhanViendto();
				nhanViendto.setNguoi_id((Integer) o[0]);
				nhanViendto.setTen((String) o[1]);
				nhanViendto.setNamSinh((String) o[2]);
				nhanViendto.setSdt((String) o[3]);
				nhanViendto.setNhanvien_id((Integer) o[4]);
				nhanViendto.setMaNV((String) o[5]);
				nhanViendto.setViTri((String) o[6]);
				nhanViendto.setPhiBH((float) o[7]);

			listr.add(nhanViendto);
		}
		return listr;
		
	}

	@Override
	public List<NhanViendto> filterByName(Integer id_phongban, String name) {
		StringBuilder sql = new StringBuilder();
		sql.append("select nguoi.id as nguoi_id, nguoi.ten, nguoi.nam_sinh, nguoi.sdt, nhanvien.id as nhanvien_id, nhanvien.manv, nhanvien.vi_tri, baohiem.chi_phi from ");
		sql.append("phongban join nhanvien on phongban.id = nhanvien.phongban_id join baohiem on nhanvien.baohiem_id = baohiem.id ");
		sql.append("join nguoi on nguoi.id = nhanvien.nguoi_id where phongban.id = :id and nhanvien.status like concat('%',:statusn,'%') and nguoi.ten like concat('%',:name,'%')");
		Query query = (Query) em.createNativeQuery(sql.toString());
		query.setParameter("id", id_phongban);
		query.setParameter("statusn", "1");
		query.setParameter("name", name);
		List<Object[]> list = query.getResultList();
		List<NhanViendto> listr = new ArrayList<>();
		for(Object[] o : list) {
			NhanViendto nhanViendto = new NhanViendto();
				nhanViendto.setNguoi_id((Integer) o[0]);
				nhanViendto.setTen((String) o[1]);
				nhanViendto.setNamSinh((String) o[2]);
				nhanViendto.setSdt((String) o[3]);
				nhanViendto.setNhanvien_id((Integer) o[4]);
				nhanViendto.setMaNV((String) o[5]);
				nhanViendto.setViTri((String) o[6]);
				nhanViendto.setPhiBH((float) o[7]);
			listr.add(nhanViendto);
		}
		
		return listr;
		
	}

	@Override
	public List<NhanViendto> xinNghiPhep(Integer id) {
		StringBuilder sql = new StringBuilder();
		String status = "Chờ phê duyệt";
		sql.append("select nguoi.id as nguoi_id, nguoi.ten, nguoi.nam_sinh, nguoi.sdt, nhanvien.id, nhanvien.manv, nhanvien.vi_tri, nghiphep.son, nghiphep.thoi_gianbd, nghiphep.thoi_giankt, nghiphep.ly_do, nghiphep.status ");
		sql.append("from nhanvien join phongban on nhanvien.phongban_id = phongban.id join nghiphep on nhanvien.id = nghiphep.nhanvien_id ");
		sql.append("join nguoi on nguoi.id = nhanvien.nguoi_id where phongban.id = :id and nghiphep.status = :status");
		Query query = (Query) em.createNativeQuery(sql.toString());
		query.setParameter("id", id);
		query.setParameter("status", status);
		List<Object[]> list = query.getResultList();
		List<NhanViendto> listr = new ArrayList<>();
		for(Object[] o : list) {
			NhanViendto nhanViendto = new NhanViendto();
			nhanViendto.setNguoi_id((Integer) o[0]);
			nhanViendto.setTen((String) o[1]);
			nhanViendto.setNamSinh((String) o[2]);
			nhanViendto.setSdt((String) o[3]);
			nhanViendto.setNhanvien_id((Integer) o[4]);
			nhanViendto.setMaNV((String) o[5]);
			nhanViendto.setViTri((String) o[6]);
			nhanViendto.setSoN((float) o[7]) ;
			nhanViendto.setThoiGianBD((String) o[8]);
			nhanViendto.setThoiGianKT((String) o[9]);
			nhanViendto.setLyDo((String) o[10]);
			nhanViendto.setStatus((String) o[11]);
			listr.add(nhanViendto);
		}
		return listr;
	}

	@Override
	public List<NhanViendto> thamGiaHoatDong(Integer id_h, Integer id_p) {
		StringBuilder sql = new StringBuilder();
	
		sql.append("select nguoi.id as nguoi_id, nguoi.ten, nguoi.nam_sinh, nguoi.sdt, nhanvien.manv, nhanvien.vi_tri, nhanvientg.id from nhanvientg ");
		sql.append("join hoatdongdn on nhanvientg.hoatdongdn_id = hoatdongdn.id join nhanvien on nhanvientg.nhanvien_id = nhanvien.id ");
		sql.append("join nguoi on nhanvien.nguoi_id = nguoi.id join phongban on nhanvien.phongban_id = phongban.id ");
		sql.append("where hoatdongdn.id = :id_h and phongban.id = :id_p");
		Query query = (Query) em.createNativeQuery(sql.toString());
		query.setParameter("id_h", id_h);
		query.setParameter("id_p", id_p);
		List<Object[]> list = query.getResultList();
		List<NhanViendto> listr = new ArrayList<>();
		for(Object[] o : list) {
			NhanViendto nhanViendto = new NhanViendto();
			nhanViendto.setNguoi_id((Integer) o[0]);
			nhanViendto.setTen((String) o[1]);
			nhanViendto.setNamSinh((String) o[2]);
			nhanViendto.setSdt((String) o[3]);
			nhanViendto.setMaNV((String) o[4]);
			nhanViendto.setViTri((String) o[5]);
			nhanViendto.setNhanVienTG_id((Integer) o[6]);
			listr.add(nhanViendto);
		}
		return listr;
	}

}
