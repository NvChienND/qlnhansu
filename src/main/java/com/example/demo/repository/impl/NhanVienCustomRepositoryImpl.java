package com.example.demo.repository.impl;

import com.example.demo.dto.CheckNVDto;
import com.example.demo.dto.NhanViendto;
import com.example.demo.repository.NhanVienCustomRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Repository
public class NhanVienCustomRepositoryImpl implements NhanVienCustomRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<CheckNVDto> getCheckNhanVien(String username, String password) {
        var sql = new StringBuilder();
        sql.append(" SELECT * FROM nhanvien ");
        sql.append(" WHERE tai_khoan = :username AND mat_khau = :password  ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("username", username);
        query.setParameter("password", password);
        List<CheckNVDto> nhanViendtos = new ArrayList<>();
        List<Object[]> objects = query.getResultList();
        objects.forEach(o -> {
            var nv = new CheckNVDto();
            nv.setId(String.valueOf(o[0]));
            nv.setLoai(String.valueOf(o[1]));
            nv.setManv(String.valueOf(o[2]));
            nv.setMatKhau(String.valueOf(o[3]));
            nv.setMucLuong(String.valueOf(o[4]));
            nv.setMucThue(String.valueOf(o[5]));
            Date date = (Date) o[6];
            nv.setNgaydkbhxh(String.valueOf(date));
            nv.setNgayhethan(String.valueOf(o[7]));
            nv.setNgaykyhd(String.valueOf(o[8]));
            nv.setTaikhoan(String.valueOf(o[9]));
            nv.setVitri(String.valueOf(o[10]));
            nv.setBaohiemid(String.valueOf(o[11]));
            nv.setNguoiid(String.valueOf(o[12]));
            nv.setPhongbanid(String.valueOf(o[13]));
            nv.setStatus(String.valueOf(o[14]));
            nv.setMst(String.valueOf(o[15]));
            nhanViendtos.add(nv);
        });
        return nhanViendtos;
    }
}
