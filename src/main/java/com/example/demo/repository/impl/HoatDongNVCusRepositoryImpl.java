package com.example.demo.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.model.HoatDongNV;
import com.example.demo.model.NhanVien;
import com.example.demo.repository.HoatDongNVCusRepository;
import com.example.demo.repository.NhanVienRepository;
@Repository("hoatdongnvrepo")
public class HoatDongNVCusRepositoryImpl implements HoatDongNVCusRepository {
	@PersistenceContext
	private EntityManager em;
	@Autowired
	private NhanVienRepository nhanVienRepo;
	@Override
	public List<HoatDongNV> hoatDongNV(Integer id) {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from hoatdongnv where hoatdongnv.nhanvien_id = :id");
		Query query = (Query) em.createNativeQuery(sql.toString());
		query.setParameter("id", id);
		List<HoatDongNV> list = new ArrayList<>();
		List<Object[]> listr = query.getResultList();
		for(Object[] o : listr) {
//			Optional<NhanVien> optional = nhanVienRepo.findById((Integer) o[6]);
			HoatDongNV hoatDong = new HoatDongNV();
			hoatDong.setId((Integer) o[0]);
			hoatDong.setDiemP((float) o[1]);
			hoatDong.setDiemT((float) o[2]);
			hoatDong.setSoNC((float) o[3]);
			hoatDong.setSoNP((float) o[4]);
			hoatDong.setThoiGian((String) o[5]);
//			hoatDong.setNhanvien(optional.get());
			hoatDong.setStatus((String) o[7]);
			list.add(hoatDong);
		}
		return list;
	}

	@Override
	public HoatDongNV getHoatDongNV(Integer id, String status) {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from hoatdongnv where hoatdongnv.nhanvien_id = :id and hoatdongnv.status = :status");
		Query query = (Query) em.createNativeQuery(sql.toString());
		query.setParameter("id", id);
		query.setParameter("status", status);
//		List<HoatDongNV> list = new ArrayList<>();
		Object[] o = (Object[]) query.getSingleResult();
//		for(Object[] o : listr) {
//			Optional<NhanVien> optional = nhanVienRepo.findById((Integer) o[6]);
			HoatDongNV hoatDong = new HoatDongNV();
			hoatDong.setId((Integer) o[0]);
			hoatDong.setDiemP((float) o[1]);
			hoatDong.setDiemT((float) o[2]);
			hoatDong.setSoNC((float) o[3]);
			hoatDong.setSoNP((float) o[4]);
			hoatDong.setThoiGian((String) o[5]);
//			hoatDong.setNhanvien((NhanVien) optional.get());
			hoatDong.setStatus((String) o[7]);
//			list.add(hoatDong);
//		}
		return hoatDong;
	}

}
