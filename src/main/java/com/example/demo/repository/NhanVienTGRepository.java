package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.NhanVienTG;

public interface NhanVienTGRepository extends JpaRepository<NhanVienTG, Integer> {
	
}
