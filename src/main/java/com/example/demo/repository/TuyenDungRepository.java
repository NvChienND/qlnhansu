package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.TuyenDung;

public interface TuyenDungRepository extends JpaRepository<TuyenDung, Integer> {

}
