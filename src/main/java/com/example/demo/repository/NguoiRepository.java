package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.demo.model.Nguoi;

public interface NguoiRepository extends JpaRepository<Nguoi, Integer> {
    @Query("SELECT n FROM Nguoi n WHERE n.status =1")
    List<Nguoi> getAll();

}
