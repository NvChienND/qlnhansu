package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.example.demo.model.ChiTietDN;

public interface ChiTietDNRepository extends JpaRepository<ChiTietDN, Integer> {
	@Query("SELECT c FROM ChiTietDN c WHERE c.hoatdongdn.id= :id")
	List<ChiTietDN> filterById(@Param("id") Integer id);
}
