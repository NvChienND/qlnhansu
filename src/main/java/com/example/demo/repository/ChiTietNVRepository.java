package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.ChiTietNV;

public interface ChiTietNVRepository extends JpaRepository<ChiTietNV, Integer> {
	@Query("SELECT c FROM ChiTietNV c WHERE c.hoatdongnv.id = :id")
	List<ChiTietNV> getListChiTietNV(@Param("id") Integer id_h);
}
