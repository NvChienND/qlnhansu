package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.LichLV;

public interface LichLVRepository extends JpaRepository<LichLV, Integer> {
	
}
