package com.example.demo.repository;

import com.example.demo.dto.CheckNVDto;
import com.example.demo.dto.NhanViendto;

import java.util.List;

public interface NhanVienCustomRepository {
    List<CheckNVDto> getCheckNhanVien(String username, String password);
}
