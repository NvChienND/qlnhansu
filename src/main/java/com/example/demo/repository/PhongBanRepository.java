package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.model.PhongBan;

public interface PhongBanRepository extends JpaRepository<PhongBan, Integer> {
	@Query("SELECT p FROM PhongBan p WHERE p.status=1")
	List<PhongBan> getAll();
}
