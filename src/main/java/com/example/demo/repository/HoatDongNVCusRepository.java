package com.example.demo.repository;

import java.util.List;

import com.example.demo.model.HoatDongNV;

public interface HoatDongNVCusRepository {
	List<HoatDongNV> hoatDongNV(Integer id);
	
	HoatDongNV getHoatDongNV(Integer id, String status);
}
