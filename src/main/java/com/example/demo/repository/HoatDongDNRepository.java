package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.HoatDongDN;

public interface HoatDongDNRepository extends JpaRepository<HoatDongDN, Integer> {

}
