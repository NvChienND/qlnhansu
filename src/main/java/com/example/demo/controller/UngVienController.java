package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.UngViendto;
import com.example.demo.model.Nguoi;
import com.example.demo.model.UngVien;
import com.example.demo.service.UngVienService;


@RestController
@RequestMapping(value = "/api/ungvien")
@CrossOrigin("*")

public class UngVienController {
	@Autowired
	private UngVienService ungVienService;
	
	@GetMapping("/find-ungvien-byid")
	public UngVien findById(@RequestParam("id") Integer id) {
		return ungVienService.getUngVien(id);
	}
	
	@GetMapping("/filter-ungvien-byname")
	public List<UngViendto> filterByName(@RequestParam("name") String name){
		return ungVienService.filterByName(name);
	}
	
	@GetMapping("/get-all-ungvien")
	public List<UngViendto> getAllUngVien(){
		return ungVienService.getAllUngVien();
	}
	
	@PostMapping("/create-ungvien")
	public String createUngVien(@RequestBody Nguoi nguoi, @RequestParam("vitriut") String viTriUT,
			@RequestParam("nguoi_dc") String nguoiDC, @RequestParam("sdt_nguoidc") String sdtNguoiDC ) {
		ungVienService.saveUngVien(nguoi, viTriUT, nguoiDC, sdtNguoiDC);
		return "Create successful!";
	}
	
	@DeleteMapping("/delete-ungvien")
	public String deleteUngVien(@RequestParam("nguoi_id") Integer nguoi_id, @RequestParam("id") Integer id) {
		ungVienService.deleteUngVien(nguoi_id,id);
		return "Delete successful!";
	}
	
	@PutMapping("/update-ungvien")
	public String updateUngVien(@RequestBody Nguoi nguoi, @RequestBody UngVien ungVien) {
		ungVienService.updateUngVien(nguoi, ungVien);
		return "Update successful!";
	}

}
