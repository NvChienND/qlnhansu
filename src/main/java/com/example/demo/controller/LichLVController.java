package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.LichLV;
import com.example.demo.service.LichLVService;

@RestController
@RequestMapping(value = "/api/lichlv")
@CrossOrigin("*")
public class LichLVController {
	@Autowired
	private LichLVService lichLVService;
	
	@GetMapping("/get-all-lichLV")
	public List<LichLV> getAllLichLV(){
		return lichLVService.getAllLichLV();
	}
	@PostMapping("/create-lichLV")
	public String createLichLV(@RequestBody LichLV lichLV) {
		lichLVService.saveLichLV(lichLV);
		return "Create successful!";
	}
	@DeleteMapping("/delete-lichLV")
	public String deleteLichLV(@RequestParam("id") Integer id) {
		lichLVService.deleteLichLV(id);
		return "Delete successful!";
	}
	@PutMapping("/update-lichLV")
	public String updateLichLV(@RequestBody LichLV lichLV) {
		lichLVService.updateLichLV(lichLV);
		return "Update successful!";
	}
}
