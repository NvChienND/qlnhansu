package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.XinNghiPhep;
import com.example.demo.service.NghiPhepService;

@RestController
@RequestMapping(value = "/api-nghiphep")
@CrossOrigin("*")
public class NghiPhepController {
	@Autowired
	private NghiPhepService nghiPhepService;
	
	@PostMapping("/create-nghiphep")
	String createNghiPhep(@RequestParam("nhanvien_id") Integer id, @RequestParam("songaynghi") float soN, @RequestParam("thoigianbd") String thoiGianBD,
			@RequestParam("thoigiankt") String thoiGianKT, @RequestParam("lydo") String lyDo) {
		nghiPhepService.saveNghiPhep(id, soN, thoiGianBD, thoiGianKT, lyDo);
		return "Successful!";
	}
	@PutMapping("/update-nghiphep")
	XinNghiPhep updateNghiPhep(XinNghiPhep xinNghiPhep) {
		return nghiPhepService.updateNghiPhep(xinNghiPhep);
	}
	@DeleteMapping("/delete-nghiphep")
	String deleteNghiPhep(Integer id) {
		nghiPhepService.deleteNghiPhep(id);
		return "Successful!";
	}
	@GetMapping("/get-nghiphep-nhanvien")
	List<XinNghiPhep> nghiPhepOfNhanVien(@RequestParam("id_nhanvien") Integer id){
		return nghiPhepService.nghiPhepOfNhanVien(id);
	}
}
