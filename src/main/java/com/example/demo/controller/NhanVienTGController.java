package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.NhanViendto;
import com.example.demo.model.NhanVienTG;
import com.example.demo.service.NhanVienTGService;

@RestController
@RequestMapping(value = "/api-nhanvientg")
@CrossOrigin("*")
public class NhanVienTGController {
	@Autowired
	private NhanVienTGService nhanVienTGService;
	
	@GetMapping("/get-nhanvientg")
	List<NhanViendto> getNhanVienTGHD(@RequestParam("id_hoatdong") Integer id_h, @RequestParam("id_phongban") Integer id_p){
		return nhanVienTGService.getNhanVienOfHD(id_h, id_p);
	}
	@PostMapping("/create-nhanvientg")
	String createNhanVienTG(@RequestParam("id_nhanvien") Integer id_nhanvien, @RequestParam("id_hoatdongdn") Integer id_hoatdongdn) {
		nhanVienTGService.saveNhanVienTG(id_nhanvien, id_hoatdongdn);
		return "Successful!";
	}
//	@PutMapping("/update-nhanvientg")
//	String updateNhanVienTG(@RequestBody NhanVienTG nhanVienTG) {
//		nhanVienTGService.updateNhanVienTG(nhanVienTG);
//		return "Successful!";
//	}
	@DeleteMapping("/delete-nhanvientg")
	String deleteNhanVienTg(@RequestParam("id") Integer id) {
		nhanVienTGService.deleteNhanVienTG(id);
		return "Successful!";
	}
}
