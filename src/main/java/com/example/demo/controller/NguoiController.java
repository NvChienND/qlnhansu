package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Nguoi;
import com.example.demo.service.NguoiService;

@RestController
@RequestMapping(value = "/api/nguoi")
@CrossOrigin("*")

public class NguoiController {
	@Autowired
	private NguoiService nguoiService;
	
	@PostMapping("/create-nguoi")
	public String createNguoi(@RequestBody Nguoi nguoi) {
		nguoiService.saveNguoi(nguoi);
		return "Successful!";
	}
	@GetMapping("/get-nguoi")
	public String getNguoi(@RequestParam("id") Integer id) {
		nguoiService.getNguoi(id);
		return "Successful!";
	}
	@PutMapping("/update-nguoi")
	public String updateNguoi(@RequestBody Nguoi nguoi) {
		nguoiService.updateNguoi(nguoi);
		return "Successful!";
	}
}
