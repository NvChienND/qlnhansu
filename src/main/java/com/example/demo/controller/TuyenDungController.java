package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.TuyenDung;
import com.example.demo.service.TuyenDungService;

@RestController
@RequestMapping(value = "/api/tuyendung")
@CrossOrigin("*")
public class TuyenDungController {
	@Autowired
	private TuyenDungService tuyenDungService;
	
	@PostMapping("/create/tuyendung")
	public String createTuyenDung(@RequestParam("phongban_id") Integer phongBan_id, @RequestParam("lichlv_id") Integer lichLV_id, @RequestParam("sol") int soL,
			@RequestParam("vitritd") String viTriTD, @RequestParam("thoigianbc") String thoiGianBC) {
		tuyenDungService.saveTuyenDung(phongBan_id, lichLV_id, soL, viTriTD, thoiGianBC);
		return "Successful!";
	}
	@PutMapping("/update/tuyendung")
	public String updateTuyenDung(@RequestBody TuyenDung tuyenDung) {
		tuyenDungService.updateTuyenDung(tuyenDung);
		return "Successful!";
	}
	@DeleteMapping("/delete/tuyendung")
	public String deleteTuyenDung(@RequestParam("id") Integer id) {
		tuyenDungService.deleteTuyenDung(id);
		return "Successful!";
	}
	}
