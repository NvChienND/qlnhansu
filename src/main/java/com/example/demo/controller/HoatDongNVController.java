package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.HoatDongNV;
import com.example.demo.service.HoatDongNVService;

@RestController
@RequestMapping(value = "/api/hoatdongnv")
@CrossOrigin("*")
public class HoatDongNVController {

	@Autowired
	private HoatDongNVService hoatDongNVService;
	
	@PostMapping("/create-hoatdongnv")
	String createHoatDongNV(@RequestParam("id_nhanvien") Integer id_n, @RequestParam("id_hoatdong") Integer id_h, @RequestParam("thoigian") String thoiGian) {
		hoatDongNVService.saveHoatDongNV(id_n, id_h, thoiGian);
		return "Successful!";
	}
	@GetMapping("/get-hoatdongnv")
	HoatDongNV getHoatDongNV(@RequestParam("id_nhanvien") Integer id_n, @RequestParam("status") String status) {
		return hoatDongNVService.getHoatDongNV(id_n, status);
	}
	@GetMapping("/get-list-hoatdongdn")
	List<HoatDongNV> getListHoatDongNV(@RequestParam("id_nhanvien") Integer id_n){
		return hoatDongNVService.hoatDongNV(id_n);
	}
	@PutMapping("/chamcong-nhanvien")
	String chamCongNV(@RequestBody List<Integer> listId) {
		hoatDongNVService.chamCongNV(listId);
		return "Successful!";
	}
	@PutMapping("/update-nhanvien")
	String updateNhanVien(@RequestParam("id") Integer id, @RequestParam("diemt") float diemT, @RequestParam("diemp") float diemP, @RequestParam("nghiphep") float soNP) {
		hoatDongNVService.updateHoatDongNV(id, diemT, diemP, soNP);
		return "Successful!";
	}
	@PutMapping("/update-status")
	String updateStatus(@RequestParam("id") Integer id){
		hoatDongNVService.updateStatus(id);
		return "Successful!";
	}
}
