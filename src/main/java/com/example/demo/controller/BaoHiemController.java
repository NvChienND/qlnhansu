package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.BaoHiem;
import com.example.demo.service.BaoHiemService;

@RestController
@RequestMapping(value = "/api/baohiem")
@CrossOrigin("*")

public class BaoHiemController {
	
	@Autowired(required=true)
	private BaoHiemService baoHiemService;
	
	@GetMapping("/get-all-baohiem")
	public List<BaoHiem> getAllBaoHiem() {
		return baoHiemService.getAllBaoHiem();
	}
	
	@PostMapping("/create-baohiem")
	public String createBaoHiem(@RequestBody BaoHiem baoHiem) {
		baoHiemService.saveBaoHiem(baoHiem);
		return "Create successful!";
	}
	
	@PutMapping("/update-baohiem")
	public String updateBaoHiem(@RequestBody BaoHiem baoHiem) {
		baoHiemService.updateBaoHiem(baoHiem);
		return "Update successful!";
	}
	
	@DeleteMapping("/delete-baohiem")
	public String deleteBaoHiem(@RequestParam("id") Integer id) {
		baoHiemService.deleteBaoHiem(id);
		return "Delete successful!";
	}

}
