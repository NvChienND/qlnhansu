package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.HoatDongDN;
import com.example.demo.service.HoatDongDNService;

@RestController
@RequestMapping(value = "/api/hoatdongdn")
@CrossOrigin("*")

public class HoatDongDNController {
	@Autowired
	private HoatDongDNService hoatDongDNService;
	
	@GetMapping("/get-allhd")
	public List<HoatDongDN> getAll() {

		return hoatDongDNService.getAllHoatDong();
	}
	
	@PostMapping("/create-hd")
	public String createHoatDong(@RequestBody HoatDongDN hoatDongDN) {
		hoatDongDNService.saveHoatDong(hoatDongDN);
		return "Successful!";
	}
	@PutMapping("/update-hoatdong")
	public String updateHoatDong(@RequestBody HoatDongDN hoatDongDN) {
		hoatDongDNService.updateHoatDong(hoatDongDN);
		return "Successful!";
	}
	@PutMapping("/delete-hoatdong")
	public String deleteHoatDong(@RequestParam("id") Integer id) {
		hoatDongDNService.deleteHoatDong(id);
		return "Successful!";
	}
	@PutMapping("/capkinhphi-hoatdong")
	public String capKinhPhi(@RequestParam("id") Integer id, @RequestParam("kinhphi") int kinhPhi) {
		hoatDongDNService.capKinhPhi(id, kinhPhi);
		return "Successful!";
	}
}
