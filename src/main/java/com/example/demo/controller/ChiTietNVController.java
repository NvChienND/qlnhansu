package com.example.demo.controller;

import com.example.demo.model.ChiTietNV;
import com.example.demo.service.ChiTietNVServive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/chitietnv")
@CrossOrigin("*")
public class ChiTietNVController {
    @Autowired
    private ChiTietNVServive chitietNVService;

    @PostMapping("/create-chitietnv")
    String createChiTietNV(@RequestParam("id_h") Integer id, @RequestParam("thoigian") String thoiGian, @RequestParam("loai") String loai
            ,@RequestParam("ghichu") String ghiChu, @RequestParam("sodiem") float soDiem ){
        chitietNVService.saveChiTietNV(id,thoiGian, loai, ghiChu, soDiem);
        return "Successful!";
    }
    @GetMapping("/get-all")
    List<ChiTietNV> getAll(@RequestParam("id_h") Integer id){
        return chitietNVService.getChiTietNV(id);
    }
}
