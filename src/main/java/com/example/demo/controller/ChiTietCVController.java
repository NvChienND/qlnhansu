package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.ChiTietCV;
import com.example.demo.service.ChiTietCVService;

@RestController
@RequestMapping(value = "/api/chitietcv")
@CrossOrigin("*")
public class ChiTietCVController {
	@Autowired
	private ChiTietCVService chiTietCVService;
	
	@PostMapping("/create-chitietcv")
	public String createChiTietCV(@RequestParam("phongban_id") Integer phongBan_id, @RequestParam("lichlv_id") Integer lichLV_id,
			@RequestParam("tencv") String tenCV, @RequestParam("thoigianbc") String thoiGianBC) {
		chiTietCVService.saveChiTietCV(phongBan_id, lichLV_id, tenCV, thoiGianBC);
		return "Successful!";
	}
	@PutMapping("/update-chitietcv")
	public String updateChiTietCV(@RequestBody ChiTietCV chiTietCV) {
		chiTietCVService.updateChiTietCV(chiTietCV);
		return "Successful!";
	}
	@DeleteMapping("/delete-chitietcv")	
	public String deleteChiTietCV(@RequestParam("id") Integer id) {
		chiTietCVService.deleteChiTietCV(id);
		return "Successful!";
	}
}
