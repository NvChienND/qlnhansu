package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.PhongBan;
import com.example.demo.service.PhongBanService;

@RestController
@RequestMapping(value = "/api/phongban")
@CrossOrigin("*")

public class PhongBanController {
	@Autowired
	private PhongBanService phongBanService;
	
	@GetMapping("/get-all-phongban")
	public List<PhongBan> getAllPhongBan(){
		return phongBanService.getAllPhongBan();
	}
	
	@PostMapping("/create-phongban")
	public String createPhongBan(@RequestBody PhongBan phongBan) {
		phongBanService.savePhongBan(phongBan);
		return "Create successful!";
	}
	
	@PutMapping("/update-phongban")
	public String updatePhongBan(@RequestBody PhongBan phongBan) {
		phongBanService.updatePhongBan(phongBan);
		return "Update successful!";
		
	}
	
	@DeleteMapping("/delete-phongban")
	public String deletePhongBan(@RequestParam("id") Integer id) {
		phongBanService.deletePhongBan(id);
		return "Delete successful!";
		
	}

}
