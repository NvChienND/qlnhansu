package com.example.demo.controller;

import java.time.LocalDate;
import java.util.List;

import com.example.demo.dto.CheckNVDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.BangLuong;
import com.example.demo.dto.NhanViendto;
import com.example.demo.model.Nguoi;
import com.example.demo.model.NhanVien;
import com.example.demo.service.NhanVienService;

@RestController
@RequestMapping(value = "/api/nhanvien")
@CrossOrigin("*")

public class NhanVienController {
	@Autowired
	private NhanVienService nhanVienService;
	
	@PostMapping("/create-nhanvien")
	public String createNhanVien(@RequestParam("nguoi_id") Integer nguoi_id, @RequestParam("phongban_id") Integer phongBan_id, @RequestParam("baohiem_id") Integer baoHiem_id
			,@RequestParam("manv") String maNV, @RequestParam("vt") String viTri, @RequestParam("ml") float mucLuong, @RequestParam("mst") String mST,
			@RequestParam("mt") float mucThue, @RequestParam("nbh") String ngayDKBHXH, @RequestParam("nkhd") String ngayKyHD, @RequestParam("nhhd") String ngayHetHanHD,
			@RequestParam("tk") String taiKhoan, @RequestParam("mk") String matKhau, @RequestParam("loai") String loai) {
		LocalDate ld = LocalDate.parse(ngayDKBHXH);
		nhanVienService.saveNhanVien(nguoi_id, phongBan_id, baoHiem_id, maNV, viTri, mucLuong, mST, mucThue, ld, ngayKyHD, ngayHetHanHD, taiKhoan, matKhau, loai);
		
		return "Successful!";
	}
	@PostMapping("/create-new-nhanvien")
	public String createNewNhanVien(@RequestBody Nguoi nguoi, @RequestParam("phongban_id") Integer phongBan_id, @RequestParam("baohiem_id") Integer baoHiem_id
			,@RequestParam("manv") String maNV, @RequestParam("vt") String viTri, @RequestParam("ml") float mucLuong, @RequestParam("mst") String mST,
			@RequestParam("mt") float mucThue, @RequestParam("nbh") String ngayDKBHXH, @RequestParam("nkhd") String ngayKyHD, @RequestParam("nhhd") String ngayHetHanHD,
			@RequestParam("tk") String taiKhoan, @RequestParam("mk") String matKhau, @RequestParam("loai") String loai) {
		LocalDate ld = LocalDate.parse(ngayDKBHXH);
		nhanVienService.saveNewNhanVien(nguoi, phongBan_id, baoHiem_id, maNV, viTri, mucLuong, mST, mucThue, ld, ngayKyHD, ngayHetHanHD, taiKhoan, matKhau, loai);
		
		return "Successful!";
	}
	@PutMapping("/update-nhanvien")
	public String updateNhanVien(@RequestBody NhanVien nhanVien, @RequestParam("phongban_id") Integer phongBan_id, @RequestParam("baohiem_id") Integer baoHiem_id) {
		nhanVienService.updateNhanVien(nhanVien, baoHiem_id, phongBan_id);
		return "Successful!";
	}
	@PutMapping("/delete-nhanvien")
	public String deleteNhanVien(@RequestParam("id") Integer id) {
		nhanVienService.deleteNhanVien(id);
		return "Successful!";
	}
	@GetMapping("/get-nhanvien-of-phongban")
	public List<NhanViendto> getNhanVienOfPhongBan(@RequestParam("id_phongban") Integer id, @RequestParam("status") String status){
		return nhanVienService.getNhanVienOfPhongBan(id,status);
	}
	@GetMapping("/filter-nhanvien-by-name")
	public List<NhanViendto> filterNhanVienByName(@RequestParam("id_phongban") Integer id, @RequestParam("status") String status, @RequestParam("name") String name){
		return nhanVienService.filterNhanVienByName(id, status, name);
	}
	@GetMapping("/check-nhanvien")
	public List<CheckNVDto> checkNhanVien(@RequestParam("tai_khoan") String taiKhoan, @RequestParam("mat_khau") String matKhau){
		return nhanVienService.getCheckNhanVien(taiKhoan, matKhau);
	}
	@GetMapping("/get-nhanvien-xinnghiphep")
	public List<NhanViendto> getNhanVienXinNghiPhep(@RequestParam("phongban_id") Integer phongBan_id){
		return nhanVienService.getNhanVienXinNghiPhep(phongBan_id);
	}
	@GetMapping("/tinhluong")
	public BangLuong tinhLuongNV(@RequestParam("id_phongban") Integer id_p, @RequestParam("id_nguoi") Integer id_nn, @RequestParam("id_nhanvien") Integer id_n, @RequestParam("id_hoatdong") Integer id_h) {
		return nhanVienService.tinhLuong(id_p, id_nn, id_n, id_h);
	}
	@GetMapping("/get-nhanvien")
	public NhanVien nhanVien(@RequestParam("id") Integer id) {
		return nhanVienService.getNhanVienById(id);
	}
	@GetMapping("/get-all")
	public List<NhanVien> getAll(){
		return nhanVienService.getAll();
	}
}
