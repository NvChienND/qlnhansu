package com.example.demo.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.ChiTietDN;
import com.example.demo.service.ChiTietDNService;

@RestController
@RequestMapping(value = "/api/chitietdn")
@CrossOrigin("*")
public class ChiTietDNController {
	@Autowired
	private ChiTietDNService chiTietDNService;
	
	@GetMapping("/get-chitietdn")
	public List<ChiTietDN> getChiTietDN(@RequestParam("id") Integer id){

		return chiTietDNService.filterById(id);
	}
	@PostMapping("/create-chitietdn")
	public String createChiTietDN(@RequestParam("hoatdongdn_id") Integer hoatDongDN_id, @RequestParam("chiphidc") int chiPhiDC,
			@RequestParam("thoigian") String thoiGian) {
		LocalDate ld = LocalDate.parse(thoiGian);
		chiTietDNService.saveChiTietDN(hoatDongDN_id, chiPhiDC, ld);
		return "Successful!";
	}
	@PutMapping("/update-chitietdn")
	public String updateChiTietDN(@RequestBody ChiTietDN chiTietDN) {
		chiTietDNService.updateChiTietDN(chiTietDN);
		return "Successful!";
	}
	@DeleteMapping("/delete-chitietdn")
	public String deleteChiTietDN(@RequestParam("id") Integer id) {
		chiTietDNService.deleteChiTietDN(id);
		return "Successful!";
	}
}
