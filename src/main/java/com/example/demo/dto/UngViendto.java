package com.example.demo.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UngViendto implements Serializable {
	private Integer nguoi_id;
	private String ten;
	private Integer ungvien_id;
	private String viTriUT;
	private String nguoiDC;
	private String sDTnguoiDC;
	
}
