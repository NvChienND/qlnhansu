package com.example.demo.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class NhanViendto implements Serializable {
//    private String id;
//    private String loai;
//    private String manv;
//    private String matKhau;
//    private String mucLuong;
//    private String mucThue;
//    private String ngaydkbhxh;
//    private String ngayhethan;
//    private String ngaykyhd;
//    private String taikhoan;
//    private String vitri;
//    private String baohiemid;
//    private String nguoiid;
//    private String phongbanid;
//    private String status;
//    private String mst;
private Integer nguoi_id;
    private String ten;
    private String namSinh;
    private String sdt;
    private Integer nhanvien_id;
    private String maNV;
    private String viTri;
    private float soN;//bỏ
    private String thoiGianBD;//bỏ
    private String thoiGianKT;//bỏ
    private String lyDo;//bỏ
    private String status;//bỏ
    private Integer nhanVienTG_id;//bỏ
    private Integer hoatDongNV_id; //bỏ
    private float phiBH;//bỏ
    private Integer baoHiem_id;
    private Integer phongBan_id;
}
