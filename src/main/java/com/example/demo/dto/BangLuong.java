package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BangLuong {
	private String ten;
	private String namSinh;
	private String sdt;
	private String email;
	private String maNV;
	private String phongBan;
	private String viTri;
	private float phiBH;
	private float luongCung;
	private float thuong;
	private float phat;
	private float thue;
	private float luongTruocThue;
	private float thucNhan;
}
