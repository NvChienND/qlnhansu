package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CheckNVDto {
    private String id;
    private String loai;
    private String manv;
    private String matKhau;
    private String mucLuong;
    private String mucThue;
    private String ngaydkbhxh;
    private String ngayhethan;
    private String ngaykyhd;
    private String taikhoan;
    private String vitri;
    private String baohiemid;
    private String nguoiid;
    private String phongbanid;
    private String status;
    private String mst;
}
