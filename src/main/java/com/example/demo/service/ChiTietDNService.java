package com.example.demo.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.ChiTietDN;
public interface ChiTietDNService {
	String saveChiTietDN(Integer hoatDongDN_id, int chiPhiDC, LocalDate thoiGian);
	
	List<ChiTietDN> filterById(Integer hoatDongDN_id);
	
	ChiTietDN updateChiTietDN(ChiTietDN chiTietDN);
	
	String deleteChiTietDN(Integer id);
	
}
