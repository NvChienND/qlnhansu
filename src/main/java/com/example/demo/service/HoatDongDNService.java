package com.example.demo.service;

import java.util.List;

import com.example.demo.model.HoatDongDN;

public interface HoatDongDNService {
	void saveHoatDong(HoatDongDN hoatDongDN);
	
	HoatDongDN updateHoatDong(HoatDongDN hoatDongDN);
	
	String deleteHoatDong(Integer id);

	List<HoatDongDN> getAllHoatDong();
	
	String capKinhPhi(Integer id, int kinhPhi);
	
	
}
