package com.example.demo.service;

import com.example.demo.model.ChiTietCV;

public interface ChiTietCVService { 
	String saveChiTietCV(Integer phongBan_id, Integer lichLV_id, String tenCV, String thoiGianBC);
	
	ChiTietCV updateChiTietCV(ChiTietCV chiTietCV);
	
	String deleteChiTietCV(Integer id);

}
