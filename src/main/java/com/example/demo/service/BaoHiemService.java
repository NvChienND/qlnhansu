package com.example.demo.service;

import java.util.List;

import com.example.demo.model.BaoHiem;

public interface BaoHiemService {
	
	void saveBaoHiem(BaoHiem baoHiem);
	
	List<BaoHiem> getAllBaoHiem();
	
	BaoHiem updateBaoHiem(BaoHiem baoHiem);
	
	String deleteBaoHiem(Integer id);
}
