package com.example.demo.service;

import com.example.demo.model.TuyenDung;

public interface TuyenDungService {
	String saveTuyenDung(Integer phongBan_id, Integer lichLV_id, int soL, String vitriTD, String thoiGianBC);
	
	TuyenDung updateTuyenDung(TuyenDung tuyenDung);
	
	String deleteTuyenDung(Integer id);
}
