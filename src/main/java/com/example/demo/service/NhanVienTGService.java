package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.NhanViendto;
import com.example.demo.model.NhanVienTG;

public interface NhanVienTGService {
	void saveNhanVienTG(Integer id_nhanvien, Integer id_hoatdongdn);
	
	List<NhanViendto> getNhanVienOfHD(Integer id_h, Integer id_p);
	
//	NhanVienTG updateNhanVienTG(NhanVienTG nhanVienTG);
	
	void deleteNhanVienTG(Integer id);
}
