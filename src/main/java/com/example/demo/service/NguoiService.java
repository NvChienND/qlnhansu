package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Nguoi;

public interface NguoiService {
	void saveNguoi(Nguoi nguoi);
	
	Nguoi getNguoi(Integer id);
	
	Nguoi updateNguoi(Nguoi nguoi);
	
}
