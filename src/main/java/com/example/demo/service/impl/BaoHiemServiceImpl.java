package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.BaoHiem;
import com.example.demo.repository.BaoHiemRepository;
import com.example.demo.service.BaoHiemService;
@Service
public class BaoHiemServiceImpl implements BaoHiemService {
	@Autowired
	private BaoHiemRepository baoHiemrepo;

	@Override
	public void saveBaoHiem(BaoHiem baoHiem) {
		baoHiem.setStatus("1");
		baoHiemrepo.save(baoHiem);
	}

	@Override
	public List<BaoHiem> getAllBaoHiem() {
		return baoHiemrepo.getAll();
	}

	@Override
	public BaoHiem updateBaoHiem(BaoHiem baoHiem) {
		Optional<BaoHiem> optional = baoHiemrepo.findById(baoHiem.getId());
		if (optional.isEmpty()) {
			return null;
		}
		optional.get().setMuc(baoHiem.getMuc());
		optional.get().setChiPhi(baoHiem.getChiPhi());
		
		baoHiemrepo.save(optional.get());
		return optional.get();
	}

	@Override
	public String deleteBaoHiem(Integer id) {
		Optional<BaoHiem> optional = baoHiemrepo.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		optional.get().setStatus("0");
		baoHiemrepo.save(optional.get());
		return "Suscessful!";
	}

}
