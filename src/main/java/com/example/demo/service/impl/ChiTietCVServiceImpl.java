package com.example.demo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.ChiTietCV;
import com.example.demo.model.LichLV;
import com.example.demo.model.PhongBan;
import com.example.demo.repository.ChiTietCVRepository;
import com.example.demo.repository.LichLVRepository;
import com.example.demo.repository.PhongBanRepository;
import com.example.demo.service.ChiTietCVService;
@Service
public class ChiTietCVServiceImpl implements ChiTietCVService {
	@Autowired
	private ChiTietCVRepository chiTietCVRepo;
	@Autowired
	private PhongBanRepository phongBanRepo;
	@Autowired
	private LichLVRepository lichLVRepo;

	@Override
	public String saveChiTietCV(Integer phongBan_id, Integer lichLV_id, String tenCV, String thoiGianBC) {
		Optional<PhongBan> optionalp = phongBanRepo.findById(phongBan_id);
		if (optionalp.isEmpty()) {
					return null; }
		Optional<LichLV> optionall = lichLVRepo.findById(lichLV_id);
		if (optionalp.isEmpty()) {
			return null; }
		ChiTietCV chiTietCV = new ChiTietCV();
		chiTietCV.setPhongban(optionalp.get());
		chiTietCV.setLichlv(optionall.get());
		chiTietCV.setTenCV(tenCV);
		chiTietCV.setThoiGianBC(thoiGianBC);
		
		chiTietCVRepo.save(chiTietCV);
		return "Successful!";
	}

	@Override
	public ChiTietCV updateChiTietCV(ChiTietCV chiTietCV) {
		Optional<ChiTietCV> optionalc = chiTietCVRepo.findById(chiTietCV.getId());
		if (optionalc.isEmpty()) {
			return null; }
		optionalc.get().setTenCV(chiTietCV.getTenCV());
		optionalc.get().setThoiGianBC(chiTietCV.getThoiGianBC());
		
		chiTietCVRepo.save(optionalc.get());
		
		return optionalc.get();
	}

	@Override
	public String deleteChiTietCV(Integer id) {
		Optional<ChiTietCV> optionalc = chiTietCVRepo.findById(id);
		if (optionalc.isEmpty()) {
			return null; }
		chiTietCVRepo.delete(optionalc.get());
		return null;
	}
	

}
