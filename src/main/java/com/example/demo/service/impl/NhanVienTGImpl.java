package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.NhanViendto;
import com.example.demo.model.HoatDongDN;
import com.example.demo.model.NhanVien;
import com.example.demo.model.NhanVienTG;
import com.example.demo.repository.HoatDongDNRepository;
import com.example.demo.repository.NhanVienRepository;
import com.example.demo.repository.NhanVienTGRepository;
import com.example.demo.repository.PhongBanCusRepository;
import com.example.demo.service.NhanVienTGService;
@Service
public class NhanVienTGImpl implements NhanVienTGService {
	@Autowired
	private NhanVienTGRepository nhanVienTGRepo;
	@Autowired
	private NhanVienRepository nhanVienRepo;
	@Autowired
	private HoatDongDNRepository hoatDongDNRepo;
	@Autowired
	private PhongBanCusRepository phongBanCusRepo;

	@Override
	public void saveNhanVienTG(Integer id_nhanvien, Integer id_hoatdongdn) {
		Optional<NhanVien> optionaln = nhanVienRepo.findById(id_nhanvien);
		Optional<HoatDongDN> optionalh = hoatDongDNRepo.findById(id_hoatdongdn);
		NhanVienTG nhanVienTG = new NhanVienTG();
		nhanVienTG.setNhanvien(optionaln.get());
		nhanVienTG.setHoatdongdn(optionalh.get());
		nhanVienTGRepo.save(nhanVienTG);
		
	}

	@Override
	public List<NhanViendto> getNhanVienOfHD(Integer id_h, Integer id_p) {
		return phongBanCusRepo.thamGiaHoatDong(id_h, id_p);
	}

//	@Override
//	public NhanVienTG updateNhanVienTG(NhanVienTG nhanVienTG) {
//		Optional<NhanVienTG> optionaln = nhanVienTGRepo.findById(nhanVienTG.getId());
//		optionaln.get().setThoiGian(nhanVienTG.getThoiGian());
//		nhanVienTGRepo.save(optionaln.get());
//		return optionaln.get();
//	}

	@Override
	public void deleteNhanVienTG(Integer id) {
		Optional<NhanVienTG> optionaln = nhanVienTGRepo.findById(id);
		nhanVienTGRepo.delete(optionaln.get());
	}
	

}
