package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.NhanVien;
import com.example.demo.model.XinNghiPhep;
import com.example.demo.repository.NghiPhepRepository;
import com.example.demo.repository.NhanVienRepository;
import com.example.demo.service.NghiPhepService;

@Service
public class XinNghiPhepServiceImpl implements NghiPhepService{
	
	@Autowired
	private NghiPhepRepository nghiPhepRepo;
	@Autowired
	private NhanVienRepository nhanVienRepo;
	
	@Override
	public String saveNghiPhep(Integer nhanVien_id, float soN, String thoiGianBD, String thoiGianKT, String lyDo) {
		Optional<NhanVien> optional = nhanVienRepo.findById(nhanVien_id);
		XinNghiPhep xinNghiPhep = new XinNghiPhep();
		xinNghiPhep.setNhanvien(optional.get());
		xinNghiPhep.setSoN(soN);
		xinNghiPhep.setThoiGianBD(thoiGianBD);
		xinNghiPhep.setThoiGianKT(thoiGianKT);
		xinNghiPhep.setLyDo(lyDo);
		xinNghiPhep.setStatus("Chờ Phê Duyệt");
		nghiPhepRepo.save(xinNghiPhep);
		return "Successful!";
	}

	@Override
	public XinNghiPhep updateNghiPhep(XinNghiPhep xinNghiPhep) {
		Optional<XinNghiPhep> optional = nghiPhepRepo.findById(xinNghiPhep.getId());
		optional.get().setThoiGianBD(xinNghiPhep.getThoiGianBD());
		optional.get().setThoiGianKT(xinNghiPhep.getThoiGianKT());
		optional.get().setLyDo(xinNghiPhep.getLyDo());
		optional.get().setStatus(xinNghiPhep.getStatus());
		
		nghiPhepRepo.save(optional.get());
		return optional.get();
	}

	@Override
	public String deleteNghiPhep(Integer id) {
		Optional<XinNghiPhep> optional = nghiPhepRepo.findById(id);
		nghiPhepRepo.delete(optional.get());
		return "Successful!";
	}

	@Override
	public List<XinNghiPhep> nghiPhepOfNhanVien(Integer id) {
		return nghiPhepRepo.nghiPhepOfNhanVien(id);
	}

}
