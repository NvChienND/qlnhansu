package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.PhongBan;
import com.example.demo.repository.PhongBanRepository;
import com.example.demo.service.PhongBanService;
@Service
public class PhongBanServiceImpl implements PhongBanService {
	
	@Autowired
	private PhongBanRepository phongBanrepo;

	@Override
	public Void savePhongBan(PhongBan phongBan) {
		phongBan.setStatus("1");
		phongBanrepo.save(phongBan);
		return null;
	}

	@Override
	public List<PhongBan> getAllPhongBan() {
		return phongBanrepo.getAll();
	}

	@Override
	public PhongBan updatePhongBan(PhongBan phongBan) {
		Optional<PhongBan> optional = phongBanrepo.findById(phongBan.getId());
		if (optional.isEmpty()) {
			return null;
		}
		optional.get().setTen(phongBan.getTen());
		optional.get().setSoLuongNV(phongBan.getSoLuongNV());
		phongBanrepo.save(optional.get());
		return optional.get();
	}

	@Override
	public String deletePhongBan(Integer id) {
		Optional<PhongBan> optional = phongBanrepo.findById(id);
		if (optional.isEmpty()) {
			return null;
		}
		optional.get().setStatus("0");
		phongBanrepo.save(optional.get());
		return "Delete successful!";
	}
	

}
