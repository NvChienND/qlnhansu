package com.example.demo.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.demo.dto.CheckNVDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.BangLuong;
import com.example.demo.dto.NhanViendto;
import com.example.demo.model.BaoHiem;
import com.example.demo.model.HoatDongNV;
import com.example.demo.model.Nguoi;
import com.example.demo.model.NhanVien;
import com.example.demo.model.PhongBan;
import com.example.demo.repository.BaoHiemRepository;
import com.example.demo.repository.HoatDongNVCusRepository;
import com.example.demo.repository.HoatDongNVRepository;
import com.example.demo.repository.NguoiRepository;
import com.example.demo.repository.NhanVienRepository;
import com.example.demo.repository.PhongBanCusRepository;
import com.example.demo.repository.PhongBanRepository;
import com.example.demo.service.NhanVienService;

@Service
public class NhanVienServiceImpl implements NhanVienService {
    @Autowired
    private NguoiRepository nguoiRepo;
    @Autowired
    private NhanVienRepository nhanVienRepo;
    @Autowired
    private BaoHiemRepository baoHiemRepo;
    @Autowired
    private PhongBanRepository phongBanRepo;
    @Autowired
    private PhongBanCusRepository phongBanCusRepo;
    @Autowired
    private HoatDongNVRepository hoatDongNVRepo;
    @Autowired
    private HoatDongNVCusRepository hoatDongRepo;

    @Override
    public void saveNhanVien(Integer nguoi_id, Integer phongBan_id, Integer baoHiem_id, String maNV, String viTri,
                             float mucLuong, String mST, float mucThue, LocalDate ngayDKBHXH, String ngayKyHD, String ngayHetHanHD, String taiKhoan,
                             String matKhau, String loai) {
        Optional<Nguoi> optinaln = nguoiRepo.findById(nguoi_id);
        Optional<BaoHiem> optionalb = baoHiemRepo.findById(baoHiem_id);
        Optional<PhongBan> optionalp = phongBanRepo.findById(phongBan_id);

        NhanVien nhanVien = new NhanVien();
        nhanVien.setNguoi(optinaln.get());
        nhanVien.setPhongban(optionalp.get());
        nhanVien.setBaohiem(optionalb.get());
        nhanVien.setMaNV(maNV);
        nhanVien.setTaiKhoan(taiKhoan);
        nhanVien.setLoai(loai);
        nhanVien.setViTri(viTri);
        nhanVien.setMucLuong(mucLuong);
        nhanVien.setMSt(mST);
        nhanVien.setMucThue(mucThue);
        nhanVien.setNgayDKBHXH(ngayDKBHXH);
        nhanVien.setNgayKyHD(ngayKyHD);
        nhanVien.setNgayHetHanHD(ngayHetHanHD);
        nhanVien.setStatus("1");

        nhanVienRepo.save(nhanVien);
    }

    @Override
    public void saveNewNhanVien(Nguoi nguoi, Integer phongBan_id, Integer baoHiem_id, String maNV, String viTri,
                                float mucLuong, String mST, float mucThue, LocalDate ngayDKBHXH, String ngayKyHD, String ngayHetHanHD, String taiKhoan,
                                String matKhau, String loai) {
        Optional<BaoHiem> optionalb = baoHiemRepo.findById(baoHiem_id);
        Optional<PhongBan> optionalp = phongBanRepo.findById(phongBan_id);

        NhanVien nhanVien = new NhanVien();
        nhanVien.setNguoi(nguoi);
        nhanVien.setPhongban(optionalp.get());
        nhanVien.setBaohiem(optionalb.get());
        nhanVien.setMaNV(maNV);
        nhanVien.setTaiKhoan(taiKhoan);
        nhanVien.setLoai(loai);
        nhanVien.setViTri(viTri);
        nhanVien.setMucLuong(mucLuong);
        nhanVien.setMSt(mST);
        nhanVien.setMucThue(mucThue);
        nhanVien.setNgayDKBHXH(ngayDKBHXH);
        nhanVien.setNgayKyHD(ngayKyHD);
        nhanVien.setNgayHetHanHD(ngayHetHanHD);
        nhanVien.setStatus("1");

        nhanVienRepo.save(nhanVien);

    }

    @Override
    public NhanVien updateNhanVien(NhanVien nhanVien, Integer phongBan_id, Integer baoHiem_id) {
        Optional<NhanVien> optionaln = nhanVienRepo.findById(nhanVien.getId());
        Optional<BaoHiem> optionalb = baoHiemRepo.findById(baoHiem_id);
        Optional<PhongBan> optionalp = phongBanRepo.findById(phongBan_id);

        optionaln.get().setMaNV(nhanVien.getMaNV());
        optionaln.get().setBaohiem(optionalb.get());
        optionaln.get().setPhongban(optionalp.get());
        optionaln.get().setMaNV(nhanVien.getMaNV());
        optionaln.get().setTaiKhoan(nhanVien.getTaiKhoan());
        optionaln.get().setMatKhau(nhanVien.getMatKhau());
        optionaln.get().setLoai(nhanVien.getLoai());
        optionaln.get().setViTri(nhanVien.getViTri());
        optionaln.get().setMucLuong(nhanVien.getMucLuong());
        optionaln.get().setMSt(nhanVien.getMSt());
        optionaln.get().setMucThue(nhanVien.getMucThue());
        optionaln.get().setNgayDKBHXH(nhanVien.getNgayDKBHXH());
        optionaln.get().setNgayKyHD(nhanVien.getNgayKyHD());
        optionaln.get().setNgayHetHanHD(nhanVien.getNgayHetHanHD());

        nhanVienRepo.save(optionaln.get());
        return optionaln.get();
    }

    @Override
    public String deleteNhanVien(Integer id) {
        Optional<NhanVien> optionaln = nhanVienRepo.findById(id);

        optionaln.get().setStatus("0");
        nhanVienRepo.save(optionaln.get());

        return "Successful!";
    }

    @Override
    public NhanVien getNhanVienById(Integer id) {
        Optional<NhanVien> optionaln = nhanVienRepo.findById(id);
        return optionaln.get();
    }

    @Override
    public List<NhanViendto> getNhanVienOfPhongBan(Integer id, String status) {
        if (status.isEmpty()) {
            return phongBanCusRepo.getNhanVienOfPhongBan(id);
        } else {
            List<NhanViendto> listf = new ArrayList<>();
            List<NhanViendto> listr = new ArrayList<>();
            listf = phongBanCusRepo.getNhanVienOfPhongBan(id);
            for (NhanViendto n : listf) {
                Integer idn = n.getNhanvien_id();
                Optional<HoatDongNV> optional = Optional.ofNullable(hoatDongRepo.getHoatDongNV(idn, status));
                Optional<NhanViendto> optionaln = Optional.ofNullable(n);
                optionaln.get().setHoatDongNV_id(optional.get().getId());
                listr.add(optionaln.get());
            }
            return listr;
        }
    }

    @Override
    public List<NhanViendto> filterNhanVienByName(Integer id_phongban, String status, String name) {
        if (status.isEmpty()) {
            return phongBanCusRepo.filterByName(id_phongban, name);
        } else {
            List<NhanViendto> listf = new ArrayList<>();
            List<NhanViendto> listr = new ArrayList<>();
            listf = phongBanCusRepo.filterByName(id_phongban, name);
            for (NhanViendto n : listf) {
                Integer idn = n.getNhanvien_id();
                Optional<HoatDongNV> optional = Optional.ofNullable(hoatDongRepo.getHoatDongNV(idn, status));
                Optional<NhanViendto> optionaln = Optional.ofNullable(n);
                optionaln.get().setHoatDongNV_id(optional.get().getId());
                listr.add(optionaln.get());
            }
            return listr;
        }
    }

    @Override
    public List<CheckNVDto> getCheckNhanVien(String taiKhoan, String matKhau) {
        return nhanVienRepo.getCheckNhanVien(taiKhoan, matKhau);
    }

    @Override
    public List<NhanViendto> getNhanVienXinNghiPhep(Integer phongBan_id) {
        return phongBanCusRepo.xinNghiPhep(phongBan_id);
    }

    @Override
    public BangLuong tinhLuong(Integer id_p, Integer id_nn, Integer id_n, Integer id_h) {
        Optional<PhongBan> optionalp = phongBanRepo.findById(id_p);
        Optional<NhanVien> optionaln = nhanVienRepo.findById(id_n);
        Optional<HoatDongNV> optionalh = hoatDongNVRepo.findById(id_h);
        Optional<Nguoi> optional = nguoiRepo.findById(id_nn);

        BangLuong bangLuong = new BangLuong();
        bangLuong.setMaNV(optionaln.get().getMaNV());
        bangLuong.setTen(optional.get().getTen());
        bangLuong.setNamSinh(optional.get().getNamSinh());
        bangLuong.setEmail(optional.get().getEmail());
        bangLuong.setSdt(optional.get().getSdt());
        bangLuong.setPhongBan(optionalp.get().getTen());
        bangLuong.setViTri(optionaln.get().getViTri());
        bangLuong.setLuongCung(optionaln.get().getMucLuong());
        bangLuong.setThuong(optionalh.get().getDiemT() * 100);
        bangLuong.setPhat(optionalh.get().getDiemP() * 100);
        bangLuong.setPhiBH(optionaln.get().getBaohiem().getChiPhi());
        bangLuong.setLuongTruocThue((optionaln.get().getMucLuong()) + (optionalh.get().getDiemT() * 100) - (optionalh.get().getDiemP() * 100) - (optionaln.get().getBaohiem().getChiPhi()));
        bangLuong.setThue((optionaln.get().getMucThue()) / 100
                * ((optionaln.get().getMucLuong()) + (optionalh.get().getDiemT() * 100) - (optionalh.get().getDiemP() * 100) - (optionaln.get().getBaohiem().getChiPhi())));
        bangLuong.setThucNhan(((optionaln.get().getMucLuong()) + (optionalh.get().getDiemT() * 100) - (optionalh.get().getDiemP() * 100) - (optionaln.get().getBaohiem().getChiPhi()))
                - ((optionaln.get().getMucThue()) / 100 * ((optionaln.get().getMucLuong()) + (optionalh.get().getDiemT() * 100) - (optionalh.get().getDiemP() * 100) - (optionaln.get().getBaohiem().getChiPhi()))));
        return bangLuong;
    }

    @Override
    public List<NhanVien> getAll() {
        return nhanVienRepo.findAll();
    }


}
