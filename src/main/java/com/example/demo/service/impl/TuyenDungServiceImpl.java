package com.example.demo.service.impl;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.LichLV;
import com.example.demo.model.PhongBan;
import com.example.demo.model.TuyenDung;
import com.example.demo.repository.LichLVRepository;
import com.example.demo.repository.PhongBanRepository;
import com.example.demo.repository.TuyenDungRepository;
import com.example.demo.service.TuyenDungService;
@Service
public class TuyenDungServiceImpl implements TuyenDungService {
	@Autowired
	private PhongBanRepository phongBanRepo;
	@Autowired
	private LichLVRepository lichLVRepo;
	@Autowired
	private TuyenDungRepository tuyenDungRepo;

	@Override
	public String saveTuyenDung(Integer phongBan_id, Integer lichLV_id, int soL, String viTriTD, String thoiGianBC) {
		Optional<PhongBan> optionalp = phongBanRepo.findById(phongBan_id);
		if (optionalp.isEmpty()) {
					return null; }
		Optional<LichLV> optionall = lichLVRepo.findById(lichLV_id);
		if (optionalp.isEmpty()) {
			return null; }
		TuyenDung tuyenDung = new TuyenDung();
		tuyenDung.setPhongban(optionalp.get());
		tuyenDung.setLichlv(optionall.get());
		tuyenDung.setSoL(soL);
		tuyenDung.setViTriTD(viTriTD);
		tuyenDung.setThoiGianBC(thoiGianBC);
		tuyenDungRepo.save(tuyenDung);
		return "Successful";
	}

	@Override
	public TuyenDung updateTuyenDung(TuyenDung tuyenDung) {
		Optional<TuyenDung> optionalt = tuyenDungRepo.findById(tuyenDung.getId());
		if (optionalt.isEmpty()) {
			return null; }
		optionalt.get().setSoL(tuyenDung.getSoL());
		optionalt.get().setViTriTD(tuyenDung.getViTriTD());
		optionalt.get().setThoiGianBC(tuyenDung.getThoiGianBC());
		
		tuyenDungRepo.save(optionalt.get());
		
		return optionalt.get();
	}

	@Override
	public String deleteTuyenDung(Integer id) {
		Optional<TuyenDung> optionalt = tuyenDungRepo.findById(id);
		if (optionalt.isEmpty()) {
			return null; }
		tuyenDungRepo.delete(optionalt.get());
		return "Successful!";
	}
}
