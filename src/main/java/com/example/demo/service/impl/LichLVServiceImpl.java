package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.LichLV;
import com.example.demo.repository.LichLVRepository;
import com.example.demo.service.LichLVService;
@Service
public class LichLVServiceImpl implements LichLVService {
	
	@Autowired
	private LichLVRepository lichLVrepo;

	@Override
	public void saveLichLV(LichLV lichLV) {
		lichLVrepo.save(lichLV);	
	}

	@Override
	public List<LichLV> getAllLichLV() {
		return lichLVrepo.findAll();
	}

	@Override
	public LichLV updateLichLV(LichLV lichLV) {
		Optional<LichLV> optional = lichLVrepo.findById(lichLV.getId());
		if (optional.isEmpty()) {
			return null;
		}
		optional.get().setKieu(lichLV.getKieu());
		optional.get().setGhiChu(lichLV.getGhiChu());
		optional.get().setThoiGianBD(lichLV.getThoiGianBD());
		optional.get().setThoiGianKT(lichLV.getThoiGianKT());
		optional.get().setTrangThai(lichLV.getTrangThai());
		
		lichLVrepo.save(optional.get());
		return optional.get();
	}

	@Override
	public String deleteLichLV(Integer id) {
		LichLV lichLV = new LichLV();
		lichLV.setId(id);
		lichLVrepo.delete(lichLV);
		return "Delete successfull!";
	}

}
