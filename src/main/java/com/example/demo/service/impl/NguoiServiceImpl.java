package com.example.demo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Nguoi;
import com.example.demo.repository.NguoiRepository;
import com.example.demo.service.NguoiService;
@Service
public class NguoiServiceImpl implements NguoiService {
	@Autowired
	private NguoiRepository nguoiRepo;
	
	@Override
	public void saveNguoi(Nguoi nguoi) {
		nguoi.setStatus("1");
		nguoiRepo.save(nguoi);	
	}

	@Override
	public Nguoi getNguoi(Integer id) {
		Optional<Nguoi> optionaln = nguoiRepo.findById(id);
		if(optionaln.isEmpty()) {
			return null;
		}
		return optionaln.get();
	}

	@Override
	public Nguoi updateNguoi(Nguoi nguoi) {
		Optional<Nguoi> optionaln = nguoiRepo.findById(nguoi.getId());
		if (optionaln.isEmpty()) {
			return null;
		}
		optionaln.get().setTen(nguoi.getTen());
		optionaln.get().setNamSinh(nguoi.getNamSinh());
		optionaln.get().setSoCmnd(nguoi.getSoCmnd());
		optionaln.get().setDiachi(nguoi.getDiachi());
		optionaln.get().setSdt(nguoi.getSdt());
		optionaln.get().setBangCap(nguoi.getBangCap());
		optionaln.get().setKinhNghiem(nguoi.getKinhNghiem());
		optionaln.get().setTruongDh(nguoi.getTruongDh());
		optionaln.get().setNganhHoc(nguoi.getNganhHoc());
		optionaln.get().setSoThich(nguoi.getSoThich());
		nguoiRepo.save(optionaln.get());
		return optionaln.get();
	}

}
