package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.ChiTietNV;
import com.example.demo.model.HoatDongNV;
import com.example.demo.repository.ChiTietNVRepository;
import com.example.demo.repository.HoatDongNVRepository;
import com.example.demo.service.ChiTietNVServive;
@Service
public class ChiTietNVImpl implements ChiTietNVServive {
	@Autowired
	private HoatDongNVRepository hoatDongNVRepo;
	@Autowired
	private ChiTietNVRepository chiTietNVRepo;
	@Override
	public void saveChiTietNV(Integer id_h, String thoiGian, String loai, String ghiChu, float soD) {
		Optional<HoatDongNV> optional = hoatDongNVRepo.findById(id_h);
		ChiTietNV chiTietNV = new ChiTietNV();
		chiTietNV.setHoatdongnv(optional.get());
		chiTietNV.setThoiGian(thoiGian);
		chiTietNV.setLoai(loai);
		chiTietNV.setSoD(soD);
		chiTietNV.setGhiChu(ghiChu);
		
		chiTietNVRepo.save(chiTietNV);
		
	}

	@Override
	public List<ChiTietNV> getChiTietNV(Integer id_h) {
		return chiTietNVRepo.getListChiTietNV(id_h);
	}

}
