package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.HoatDongDN;
import com.example.demo.repository.HoatDongDNRepository;
import com.example.demo.service.HoatDongDNService;

@Service
public class HoatDongDNServiceImpl implements HoatDongDNService {
	@Autowired
	private HoatDongDNRepository hoatDongDNRepo;
	@Override
	public void saveHoatDong(HoatDongDN hoatDongDN) {
		Optional<HoatDongDN> optional = hoatDongDNRepo.findById(hoatDongDN.getId());
		optional.get().setChiPhiDC(0);
		optional.get().setStatus("open");
		hoatDongDNRepo.save(optional.get());
	}

	@Override
	public HoatDongDN updateHoatDong(HoatDongDN hoatDongDN) {
		Optional<HoatDongDN> optional = hoatDongDNRepo.findById(hoatDongDN.getId());
		if(optional.isEmpty()) {
			return null;
		}
		optional.get().setTen(hoatDongDN.getTen());
		optional.get().setChiPhi(hoatDongDN.getChiPhi());
		optional.get().setThoiGianBD(hoatDongDN.getThoiGianBD());;
		optional.get().setThoiGianKT(hoatDongDN.getThoiGianKT());
		optional.get().setChiPhiDC(hoatDongDN.getChiPhiDC());
		optional.get().setGhiChu(hoatDongDN.getGhiChu());
		
		hoatDongDNRepo.save(optional.get());
		return optional.get();
	}

	@Override
	public String deleteHoatDong(Integer id) {
		Optional<HoatDongDN> optional = hoatDongDNRepo.findById(id);
		optional.get().setStatus("close");
		hoatDongDNRepo.save(optional.get());
		return "Successful!";
	}

	@Override
	public List<HoatDongDN> getAllHoatDong() {
		return hoatDongDNRepo.findAll();
	}

	@Override
	public String capKinhPhi(Integer id, int kinhPhi) {
		Optional<HoatDongDN> optional = hoatDongDNRepo.findById(id);
		optional.get().setChiPhiDC(optional.get().getChiPhiDC() + kinhPhi);
		hoatDongDNRepo.save(optional.get());
		return "Successful!";
	}

}
