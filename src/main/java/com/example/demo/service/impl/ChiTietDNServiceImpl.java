package com.example.demo.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.ChiTietCV;
import com.example.demo.model.ChiTietDN;
import com.example.demo.model.HoatDongDN;
import com.example.demo.repository.ChiTietDNRepository;
import com.example.demo.repository.HoatDongDNRepository;
import com.example.demo.service.ChiTietCVService;
import com.example.demo.service.ChiTietDNService;

@Service
public class ChiTietDNServiceImpl implements ChiTietDNService{
	@Autowired
	private ChiTietDNRepository chiTietDNRepo;
	@Autowired
	private HoatDongDNRepository hoatDongDNRepo;

	@Override
	public String saveChiTietDN(Integer hoatDongDN_id, int chiPhiDC, LocalDate thoiGian) {
		Optional<HoatDongDN> optional = hoatDongDNRepo.findById(hoatDongDN_id);
		ChiTietDN chiTietDN = new ChiTietDN();
		chiTietDN.setHoatdongdn(optional.get());
		chiTietDN.setChiPhiDC(chiPhiDC);
		chiTietDN.setThoiGian(thoiGian);
		chiTietDNRepo.save(chiTietDN);
		return "Successful!";
	}

	@Override
	public List<ChiTietDN> filterById(Integer hoatDongDN_id) {
		return chiTietDNRepo.filterById(hoatDongDN_id);
	}

	@Override
	public ChiTietDN updateChiTietDN(ChiTietDN chiTietDN) {
		Optional<ChiTietDN> optional = chiTietDNRepo.findById(chiTietDN.getId());
		optional.get().setChiPhiDC(chiTietDN.getChiPhiDC());
		optional.get().setThoiGian(chiTietDN.getThoiGian());
		chiTietDNRepo.save(optional.get());
		return optional.get();
	}

	@Override
	public String deleteChiTietDN(Integer id) {
		Optional<ChiTietDN> optional = chiTietDNRepo.findById(id);
		chiTietDNRepo.delete(optional.get());
		return "Successful!";
	}
	
	
}
