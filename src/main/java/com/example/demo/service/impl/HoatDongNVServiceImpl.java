package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.HoatDongNV;
import com.example.demo.model.NhanVien;
import com.example.demo.repository.HoatDongDNRepository;
import com.example.demo.repository.HoatDongNVCusRepository;
import com.example.demo.repository.HoatDongNVRepository;
import com.example.demo.repository.NhanVienRepository;
import com.example.demo.service.HoatDongNVService;
@Service
public class HoatDongNVServiceImpl implements HoatDongNVService {
	@Autowired
	private HoatDongNVRepository hoatDongNVRepo;
	@Autowired
	private NhanVienRepository nhanVienRepo;
	@Autowired
	private HoatDongNVCusRepository hoatDongRepo;

	@Override
	public void saveHoatDongNV(Integer idn, Integer idh, String thoiGian) {
		Optional<NhanVien> optionaln = nhanVienRepo.findById(idn);
		if (idh==null&&thoiGian.isEmpty()){
			HoatDongNV hoatDong = new HoatDongNV();
			hoatDong.setNhanvien(optionaln.get());
			hoatDong.setThoiGian(thoiGian);
			hoatDong.setDiemP(0);
			hoatDong.setDiemT(0);
			hoatDong.setSoNC(0);
			hoatDong.setSoNP(0);
			hoatDong.setStatus("Happenning");
			hoatDongNVRepo.save(hoatDong);
		}else {
			String thoiGianL = "";
			Optional<HoatDongNV> optional = hoatDongNVRepo.findById(idh);
			String[] re = optional.get().getThoiGian().split("/");
			int t = Integer.parseInt(re[0]);
			int n = Integer.parseInt(re[1]);
			if(t<12) {
				t +=1;
				thoiGian = t + "/" + n;
			} else {
				t = 1;
				n +=1;
				thoiGian = t + "/" + n;
			}

			HoatDongNV hoatDong = new HoatDongNV();
			hoatDong.setNhanvien(optionaln.get());
			hoatDong.setThoiGian(thoiGianL);
			hoatDong.setDiemP(0);
			hoatDong.setDiemT(0);
			hoatDong.setSoNC(0);
			hoatDong.setSoNP(0);
			hoatDong.setStatus("Happenning");
			hoatDongNVRepo.save(hoatDong);
		}
	}
	@Override
	public List<HoatDongNV> chamCongNV(List<Integer> listId) {
		List<HoatDongNV> listHoatDongnv = new ArrayList<>();
		for(Integer i : listId) {
			Optional<HoatDongNV> optionalh = hoatDongNVRepo.findById(i);
			optionalh.get().setSoNC(optionalh.get().getSoNC()+1);
			listHoatDongnv.add(optionalh.get());
			hoatDongNVRepo.save(optionalh.get());
		}
		return listHoatDongnv;
	}

	@Override
	public HoatDongNV updateHoatDongNV(Integer id, float diemT, float diemP, float nghiPhep) {
		Optional<HoatDongNV> optionalh = hoatDongNVRepo.findById(id);
		if(optionalh.isEmpty()) {
			return null;
		}

		optionalh.get().setDiemT(optionalh.get().getDiemT()+diemT);
		optionalh.get().setDiemP(optionalh.get().getDiemP()+diemP);
		optionalh.get().setSoNP(optionalh.get().getSoNP()+nghiPhep);
		hoatDongNVRepo.save(optionalh.get());
		
		return optionalh.get();
	}

	@Override
	public HoatDongNV getHoatDongNV(Integer id_nhanvien, String status) {
		return hoatDongRepo.getHoatDongNV(id_nhanvien, status);
	}

	@Override
	public List<HoatDongNV> hoatDongNV(Integer id_nhanvien) {
		return hoatDongRepo.hoatDongNV(id_nhanvien);
	}

	@Override
	public HoatDongNV updateStatus(Integer id) {
		Optional<HoatDongNV> optional = hoatDongNVRepo.findById(id);
		optional.get().setStatus("Stop");
		hoatDongNVRepo.save(optional.get());
		return optional.get();
	}

}
