package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.UngViendto;
import com.example.demo.model.Nguoi;
import com.example.demo.model.UngVien;
import com.example.demo.repository.NguoiRepository;
import com.example.demo.repository.UngVienRepository;
import com.example.demo.service.UngVienService;
@Service
public class UngVienServiceImpl implements UngVienService {
	@Autowired
	private UngVienRepository ungVienrepo;
	@Autowired
	private NguoiRepository nguoirepo;

	@Override
	public String saveUngVien(Nguoi nguoi, String viTriUT, String nguoiDC, String sDTnguoiDC) {
		nguoi.setStatus("1");
		nguoirepo.save(nguoi);
	    UngVien ungVien = new UngVien();
	    ungVien.setNguoi(nguoi);
	    ungVien.setViTriUT(viTriUT);
	    ungVien.setNguoiDC(nguoiDC);
	    ungVien.setsDTnguoiDC(sDTnguoiDC);
	    ungVienrepo.save(ungVien);
	    
	    return "Successful!";
		
	}

	@Override
	public String deleteUngVien(Integer nguoi_id, Integer id) {
		Nguoi nguoi = new Nguoi();
		nguoi.setId(nguoi_id);
		UngVien ungVien = new UngVien();
		ungVien.setId(id);
		ungVienrepo.delete(ungVien);
		nguoirepo.delete(nguoi);
		return "Suscessful!";
	}

	@Override
	public UngVien updateUngVien(Nguoi nguoi, UngVien ungVien) {
		Optional<Nguoi> optionaln = nguoirepo.findById(nguoi.getId());
		if (optionaln.isEmpty()) {
			return null;
		}
		optionaln.get().setTen(nguoi.getTen());
		optionaln.get().setNamSinh(nguoi.getNamSinh());
		optionaln.get().setSoCmnd(nguoi.getSoCmnd());
		optionaln.get().setDiachi(nguoi.getDiachi());
		optionaln.get().setSdt(nguoi.getSdt());
		optionaln.get().setBangCap(nguoi.getBangCap());
		optionaln.get().setKinhNghiem(nguoi.getKinhNghiem());
		optionaln.get().setTruongDh(nguoi.getTruongDh());
		optionaln.get().setNganhHoc(nguoi.getNganhHoc());
		optionaln.get().setSoThich(nguoi.getSoThich());
		nguoirepo.save(optionaln.get());
		Optional<UngVien> optional = ungVienrepo.findById(ungVien.getId());
		if (optional.isEmpty()) {
			return null;
		}
		optional.get().setViTriUT(ungVien.getViTriUT());
		optional.get().setNguoiDC(ungVien.getNguoiDC());
		optional.get().setsDTnguoiDC(ungVien.getsDTnguoiDC());
		ungVienrepo.save(optional.get());
		return optional.get();
	}

	@Override
	public List<UngViendto> getAllUngVien() {
		List<UngVien> listu = ungVienrepo.findAll();
		List<UngViendto> listr = new ArrayList<>();
		for(UngVien u : listu) {
			UngViendto uvdto = new UngViendto();
			uvdto.setNguoi_id(u.getNguoi().getId());
			uvdto.setUngvien_id(u.getId());
			uvdto.setTen(u.getNguoi().getTen());
			uvdto.setViTriUT(u.getViTriUT());
			uvdto.setNguoiDC(u.getNguoiDC());
			uvdto.setSDTnguoiDC(u.getsDTnguoiDC());
			listr.add(uvdto);
		}
		return listr;
	}

	@Override
	public UngVien getUngVien(Integer id) {
		Optional<UngVien> optional = ungVienrepo.findById(id);
		return optional.get();
	}

	@Override
	public List<UngViendto> filterByName(String name) {
		List<UngVien> listu = ungVienrepo.findAll();
		List<UngViendto> listr = new ArrayList<>();
		for(UngVien u : listu) {
			if(u.getNguoi().getTen().equalsIgnoreCase(name)) {
				UngViendto uvdto = new UngViendto();
				uvdto.setNguoi_id(u.getNguoi().getId());
				uvdto.setUngvien_id(u.getId());
				uvdto.setTen(u.getNguoi().getTen());
				uvdto.setViTriUT(u.getViTriUT());
				uvdto.setNguoiDC(u.getNguoiDC());
				uvdto.setSDTnguoiDC(u.getsDTnguoiDC());
				listr.add(uvdto);
			}
		}
		return listr;
	}

}
