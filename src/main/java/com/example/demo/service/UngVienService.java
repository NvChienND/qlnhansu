package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.UngViendto;
import com.example.demo.model.Nguoi;
import com.example.demo.model.UngVien;

public interface UngVienService {
	String saveUngVien(Nguoi nguoi, String viTriUT, String nguoiDC, String sDTnguoiDC);
	
	String deleteUngVien(Integer id, Integer nguoi_id);
	
	UngVien updateUngVien(Nguoi nguoi, UngVien ungVien);
	
	List<UngViendto> getAllUngVien();
	
	UngVien getUngVien(Integer id);
	
	List<UngViendto> filterByName(String name);
}
