package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.NhanViendto;
import com.example.demo.model.PhongBan;

public interface PhongBanService {
	Void savePhongBan(PhongBan phongBan);
	
	List<PhongBan> getAllPhongBan();
	
	PhongBan updatePhongBan(PhongBan phongBan);
	
	String deletePhongBan(Integer id);
	
}
