package com.example.demo.service;

import java.time.LocalDate;
import java.util.List;

import com.example.demo.dto.CheckNVDto;

import com.example.demo.dto.BangLuong;
import com.example.demo.dto.NhanViendto;
import com.example.demo.model.Nguoi;
import com.example.demo.model.NhanVien;

public interface NhanVienService {
	void saveNhanVien(Integer nguoi_id, Integer phongBan_id, Integer baoHiem_id, String maNV, String viTri, float mucLuong,
			String mST, float mucThue, LocalDate ngayDKBHXH, String ngayKyHD, String ngayHetHanHD, String taiKhoan, String matKhau, String loai);
	void saveNewNhanVien(Nguoi nguoi,Integer phongBan_id, Integer baoHiem_id, String maNV, String viTri, float mucLuong,
			String mST, float mucThue, LocalDate ngayDKBHXH, String ngayKyHD, String ngayHetHanHD, String taiKhoan, String matKhau, String loai);
	NhanVien updateNhanVien(NhanVien nhanVien, Integer baoHiem_id, Integer phongBan_id);
	
	String deleteNhanVien(Integer id);
	
	NhanVien getNhanVienById(Integer id);
	
	List<NhanViendto> getNhanVienOfPhongBan(Integer id, String status);

	List<NhanViendto> filterNhanVienByName(Integer id_phongban, String status, String name);
	
	List<CheckNVDto> getCheckNhanVien(String taiKhoan, String matKhau);
	
	List<NhanViendto> getNhanVienXinNghiPhep(Integer phongBan_id);
	
	BangLuong tinhLuong(Integer id_p, Integer id_nn, Integer id_n, Integer id_h);
	
	List<NhanVien> getAll();
}

