package com.example.demo.service;

import java.util.List;

import com.example.demo.model.LichLV;

public interface LichLVService {
	
	void saveLichLV(LichLV lichLV);
	
	List<LichLV> getAllLichLV();
	
	LichLV updateLichLV(LichLV lichLV);
	
	String deleteLichLV(Integer id);
	
	
}
