package com.example.demo.service;

import java.util.List;

import com.example.demo.model.ChiTietNV;

public interface ChiTietNVServive {
	void saveChiTietNV(Integer id_h, String thoiGian, String loai, String ghiChu, float soD);
	
	List<ChiTietNV> getChiTietNV(Integer id_h);
}
