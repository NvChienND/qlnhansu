package com.example.demo.service;

import java.util.List;

import com.example.demo.model.HoatDongNV;

public interface HoatDongNVService {
	void saveHoatDongNV(Integer idn, Integer idh, String thoiGian);
	
	List<HoatDongNV> chamCongNV(List<Integer> listId);
	
	HoatDongNV updateHoatDongNV(Integer id, float diemT, float diemP, float nghiPhep);
	
	HoatDongNV getHoatDongNV(Integer id_nhanvien, String status);
	
	List<HoatDongNV> hoatDongNV(Integer id_nhanvien);

	HoatDongNV updateStatus(Integer id);
	
}
