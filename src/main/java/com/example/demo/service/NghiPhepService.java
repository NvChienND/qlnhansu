package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.NhanViendto;
import com.example.demo.model.XinNghiPhep;

public interface NghiPhepService {
	String saveNghiPhep(Integer nhanVien_id, float soN, String thoiGianBD, String thoiGianKT, String lyDo);
	
	XinNghiPhep updateNghiPhep(XinNghiPhep xinNghiPhep);
	
	String deleteNghiPhep(Integer id);
	
	List<XinNghiPhep> nghiPhepOfNhanVien(Integer id);
}
