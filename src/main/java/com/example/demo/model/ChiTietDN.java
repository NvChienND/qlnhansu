package com.example.demo.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name= "chitietdn")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class ChiTietDN implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private LocalDate thoiGian;
	private int chiPhiDC;
	
	@ManyToOne(targetEntity = HoatDongDN.class)
	@JoinColumn(name = "hoatdongdn_id")
	private HoatDongDN hoatdongdn;
}
