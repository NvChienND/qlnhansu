package com.example.demo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "lichlv")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class LichLV implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String thoiGianBD;
	private String thoiGianKT;
	private String kieu;
	private String trangThai;
	private String ghiChu;
	
	@OneToMany(targetEntity = ChiTietCV.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "lichlv")
	@JsonIgnore
	private Set<ChiTietCV> setChiTietCV;
	
	@OneToMany(targetEntity = TuyenDung.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "lichlv")
	@JsonIgnore
	private Set<TuyenDung> setTuyenDung;

}
