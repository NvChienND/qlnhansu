package com.example.demo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "baohiem")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BaoHiem implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String muc;
	private float chiPhi;
	private String status;
	
	@OneToMany(targetEntity = NhanVien.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "baohiem")
	@JsonIgnore
	private Set<NhanVien> setNhanVien;
}
