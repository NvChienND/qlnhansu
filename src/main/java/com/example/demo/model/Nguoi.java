package com.example.demo.model;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name= "nguoi")
@NoArgsConstructor
@AllArgsConstructor
public class Nguoi implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String ten;
	private String namSinh;
	private String soCmnd;
	private String sdt;
	private String email;
	private String diachi;
	private String bangCap;
	private String truongDh;
	private String nganhHoc;
	private String soThich;
	private String kinhNghiem;
	private String status;
	
	@OneToMany(targetEntity = NhanVien.class, cascade = CascadeType.ALL,
			fetch = FetchType.EAGER, mappedBy = "nguoi")
	@JsonIgnore
	private Set<NhanVien> setNhanVien;
	@OneToMany(targetEntity = UngVien.class, cascade = CascadeType.ALL,
			fetch = FetchType.EAGER, mappedBy = "nguoi")
	private Set<UngVien> setUngVien;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getNamSinh() {
		return namSinh;
	}
	public void setNamSinh(String namSinh) {
		this.namSinh = namSinh;
	}
	public String getSoCmnd() {
		return soCmnd;
	}
	public void setSoCmnd(String soCmnd) {
		this.soCmnd = soCmnd;
	}
	public String getSdt() {
		return sdt;
	}
	public void setSdt(String sdt) {
		this.sdt = sdt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDiachi() {
		return diachi;
	}
	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}
	public String getBangCap() {
		return bangCap;
	}
	public void setBangCap(String bangCap) {
		this.bangCap = bangCap;
	}
	public String getTruongDh() {
		return truongDh;
	}
	public void setTruongDh(String truongDh) {
		this.truongDh = truongDh;
	}
	public String getNganhHoc() {
		return nganhHoc;
	}
	public void setNganhHoc(String nganhHoc) {
		this.nganhHoc = nganhHoc;
	}
	public String getSoThich() {
		return soThich;
	}
	public void setSoThich(String soThich) {
		this.soThich = soThich;
	}
	public String getKinhNghiem() {
		return kinhNghiem;
	}
	public void setKinhNghiem(String kinhNghiem) {
		this.kinhNghiem = kinhNghiem;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@JsonManagedReference
	public Set<NhanVien> getSetNhanVien() {
		return setNhanVien;
	}
	public void setSetNhanVien(Set<NhanVien> setNhanVien) {
		this.setNhanVien = setNhanVien;
	}
	@JsonManagedReference
	public Set<UngVien> getSetUngVien() {
		return setUngVien;
	}
	public void setSetUngVien(Set<UngVien> setUngVien) {
		this.setUngVien = setUngVien;
	}
	
	
	

}
