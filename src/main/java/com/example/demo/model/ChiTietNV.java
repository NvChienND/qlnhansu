package com.example.demo.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Table(name= "chitietnv")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ChiTietNV implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String thoiGian;
	private String loai;
	private float soD;
	private String ghiChu;
	
	@ManyToOne(targetEntity = HoatDongNV.class)
	@JoinColumn(name = "hoatdongnv_id")
	private HoatDongNV hoatdongnv;
}
