package com.example.demo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name= "chitietcv")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class ChiTietCV implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String tenCV;
	private String thoiGianBC;
	
	@ManyToOne(targetEntity = PhongBan.class)
	@JoinColumn(name = "phongban_id")
	private PhongBan phongban;
	
	@ManyToOne(targetEntity = LichLV.class)
	@JoinColumn(name = "lichlv_id")
	private LichLV lichlv;

}
