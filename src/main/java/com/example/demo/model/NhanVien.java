package com.example.demo.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "nhanvien")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NhanVien implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String maNV;
	private String viTri;
	private float mucLuong;
	private String ngayKyHD;
	private String ngayHetHanHD;
	private float mucThue;
	private String mSt;
	private LocalDate ngayDKBHXH;
	private String taiKhoan;
	private String matKhau;
	private String loai;
	private String status;
	
	@ManyToOne(targetEntity = Nguoi.class)
	@JoinColumn(name = "nguoi_id")
	private Nguoi nguoi;
	
	@ManyToOne(targetEntity = PhongBan.class)
	@JoinColumn(name = "phongban_id")
	private PhongBan phongban;
	
	@OneToMany(targetEntity = HoatDongNV.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "nhanvien")
	@JsonIgnore
	private Set<HoatDongNV> setHoatDongNV;
	
	@OneToMany(targetEntity = NhanVienTG.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "nhanvien")
	@JsonIgnore
	private Set<NhanVienTG> setNhanVienTG;
	
	@ManyToOne(targetEntity = BaoHiem.class)
	@JoinColumn(name = "baohiem_id")
	private BaoHiem baohiem;
	
	@OneToMany(targetEntity = XinNghiPhep.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "nhanvien")
	@JsonIgnore
	private Set<XinNghiPhep> setXinNghiPhep;
	
}
