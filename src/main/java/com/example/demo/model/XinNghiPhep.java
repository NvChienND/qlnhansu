package com.example.demo.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "nghiphep")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class XinNghiPhep implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private float soN;
	private String thoiGianBD;
	private String thoiGianKT;
	private String lyDo;
	private String status;
	
	@ManyToOne(targetEntity = NhanVien.class)
	@JoinColumn(name = "nhanvien_id")
	private NhanVien nhanvien;

}
