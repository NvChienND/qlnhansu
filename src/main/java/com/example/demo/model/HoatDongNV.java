package com.example.demo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name= "hoatdongnv")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class HoatDongNV implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	/**
	 * dang 
	 */
	private String thoiGian;
	private float soNC;
	private float soNP;
//	private float soKP;
	private float diemT;
	private float diemP;
	private String status;
	
	@ManyToOne(targetEntity = NhanVien.class)
	@JoinColumn(name = "nhanvien_id")
	private NhanVien nhanvien;
	
	@OneToMany(targetEntity = ChiTietNV.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "hoatdongnv")
	@JsonIgnore
	private Set<ChiTietNV> setChiTietNV;
	
}
