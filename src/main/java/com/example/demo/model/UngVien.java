package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ungvien")
@NoArgsConstructor
@AllArgsConstructor
public class UngVien implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String viTriUT;
	private String nguoiDC;
	private String sDTnguoiDC;
	
	@ManyToOne(targetEntity = Nguoi.class)
	@JoinColumn(name = "nguoi_id")
	private Nguoi nguoi;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getViTriUT() {
		return viTriUT;
	}

	public void setViTriUT(String viTriUT) {
		this.viTriUT = viTriUT;
	}

	public String getNguoiDC() {
		return nguoiDC;
	}

	public void setNguoiDC(String nguoiDC) {
		this.nguoiDC = nguoiDC;
	}

	public String getsDTnguoiDC() {
		return sDTnguoiDC;
	}

	public void setsDTnguoiDC(String sDTnguoiDC) {
		this.sDTnguoiDC = sDTnguoiDC;
	}
	@JsonBackReference
	public Nguoi getNguoi() {
		return nguoi;
	}

	public void setNguoi(Nguoi nguoi) {
		this.nguoi = nguoi;
	}
	
	
	
}
