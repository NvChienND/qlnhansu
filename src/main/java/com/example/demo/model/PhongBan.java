package com.example.demo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "phongban")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class PhongBan implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String ten;
	private int soLuongNV;
	private String status;
	
	@OneToMany(targetEntity = NhanVien.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "phongban")
	@JsonIgnore
	private Set<NhanVien> setNhanVien;
	
	@OneToMany(targetEntity = TuyenDung.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "phongban")
	@JsonIgnore
	private Set<TuyenDung> setTuyenDung;
	
	@OneToMany(targetEntity = ChiTietCV.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "phongban")
	@JsonIgnore
	private Set<ChiTietCV> setChiTietCV;

}
