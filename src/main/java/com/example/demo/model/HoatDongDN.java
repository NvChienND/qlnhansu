package com.example.demo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name= "hoatdongdn")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class HoatDongDN implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String thoiGianBD;
	private String thoiGianKT;
	private String ten;
	private int chiPhi;
	private int chiPhiDC;
	private String ghiChu;
	private String status;

	@OneToMany(targetEntity = ChiTietDN.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "hoatdongdn")
	@JsonIgnore
	private Set<ChiTietDN> setChiTietDN;
	
	@OneToMany(targetEntity = NhanVienTG.class, cascade = CascadeType.ALL,
			fetch = FetchType.LAZY, mappedBy = "hoatdongdn")
	@JsonIgnore
	private Set<NhanVienTG> setNhanVienTG;
}
